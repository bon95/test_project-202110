export const btnBurger = () => {

    $(document).ready(function(){
        $('.js-menuBtnBurger').click(function() {
            $(this).toggleClass('is-active');
            $('.js-btnBurger').toggleClass('is-active');
            $('.js-menuText').toggleClass('is-active');
            $('.js-nav').toggleClass('is-active');
            $('.js-headerLogo').toggleClass('is-active');
        });

        // CLOSE BTN NAV SP
        // $('.js-close_nav').click(function() {
        //     $('.js-nav').slideToggle();
        //     $('.js-burger').toggleClass('is-active');
        //     $('.header_burger_inner').toggleClass('is-active');
        // })
    });
}